<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 10:30 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: String
//Example : Is Given Below



$text = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";
var_dump($text, $binary, $hello);

print "\n";

$trimmed = rtrim($text);
var_dump($trimmed);

$trimmed = rtrim($text, " \t.");
var_dump($trimmed);

$trimmed = rtrim($hello, "Hdle");
var_dump($trimmed);

$clean = rtrim($binary, "\x00..\x1F");
var_dump($clean);
