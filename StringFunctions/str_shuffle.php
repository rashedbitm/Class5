<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 10:21 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: Shuffeled String
//Example : Is Given Below


$str = '123456';
$shuffled = str_shuffle($str);


echo $shuffled;

echo "<br>";


$str = 'Smart Rashed';
$shuffled = str_shuffle($str);

echo $shuffled;
