<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 11:31 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: Modified String
//Example : Is Given Below

$foo = 'hello Rashed!';
$foo = ucwords($foo);             

$bar = 'HELLO RASHED!';
$bar = ucwords($bar);
$bar = ucwords(strtolower($bar));
