<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 10:32 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: String
//Example : Is Given Below
$input = "Rashed";
echo str_pad($input, 10);                      

echo "<br>";
echo str_pad($input, 10, "-=", STR_PAD_LEFT);  

echo "<br>";

echo str_pad($input, 10, "_", STR_PAD_BOTH);   

echo "<br>";
echo str_pad($input, 6 , "___");               
