<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 11:15 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: String
//Example : Is Given Below

$str = "Hello Rashed";

$arr1 = str_split($str);
$arr2 = str_split($str, 3);

print_r($arr1);
print_r($arr2);

