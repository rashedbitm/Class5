<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 11:33 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: String
//Example : Is Given Below

$foo = 'hello world!';
$foo = ucfirst($foo);

$bar = 'HELLO WORLD!';
$bar = ucfirst($bar);
$bar = ucfirst(strtolower($bar)); 
