<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 11:28 AM
 * Develop By: Mainuddin Rashed
 */

//Input Type: String
//Return Type: Integer
//Example : Is Given Below

$text = 'This is Smart Rashed';
echo strlen($text);

echo substr_count($text, 'is');


echo substr_count($text, 'is', 3);


echo substr_count($text, 'is', 3, 3);


echo substr_count($text, 'is', 5, 10);



$text2 = 'MainuddinRashed';
echo substr_count($text2, 'MainuddinRashed');
