<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 12:56 AM
 */

    $a = true && false;
    var_dump($a);
    
    echo "<br>";
    $b = false && true;
    var_dump($b);
    echo "<br>";
    $c = true && true;
    var_dump($c);

    echo "<br>";
    $d = false && false;
    var_dump($d);
    echo "<br>";
    $a = true || false;
    var_dump($a);
    echo "<br>";
    $b = false || true;
    var_dump($b);

    echo "<br>";
    $c = true || true;
    var_dump($c);
    echo "<br>";
    $d = false || false;
    var_dump($d);
