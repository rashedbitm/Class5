<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 5/18/2016
 * Time: 12:09 PM
 */
$a = 10;
echo 'Value of $a is :'.$a;
echo '<br />After Pre-increment value of $a ( i.e. ++$a ) is: '.++$a;
$a = 20;
echo '<br />Value of $a is :'.$a;
echo '<br />After Post-increment value of $a ( i.e. $a++ ) is: '.$a++;
$a = 30;
echo '<br />Value of $a is :'.$a;
echo '<br />After Pre-decrement value of $a ( i.e. --$a ) is: '.--$a;
$a = 40;
echo '<br />Value of $a is :'.$a;
echo '<br />After Post-decrement value of $a ( i.e. $a-- ) is: '.$a--;